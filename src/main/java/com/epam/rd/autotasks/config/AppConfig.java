package com.epam.rd.autotasks.config;

import com.epam.rd.autotasks.bean.Employee;
import com.epam.rd.autotasks.bean.Task;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for defining and managing beans for Task and Employee classes.
 */
@Configuration
public class AppConfig {

    public static final String DESCRIPTION = "New feature";
    public static final String ASSIGNEE_NAME = "John Doe";
    public static final String ASSIGNEE_POSITION = "Junior Software Engineer";
    public static final String REVIEWER_NAME = "Emily Brown";
    public static final String REVIEWER_POSITION = "Senior Software Engineer";

    /**
     * Creates a Task bean with the specified description and injected assignee and reviewer beans.
     *
     * @param assignee The assignee for the task.
     * @param reviewer The reviewer for the task.
     * @return A Task bean.
     */
    @Bean
    public Task task(@Qualifier("assignee") Employee assignee, @Qualifier("reviewer") Employee reviewer) {
        Task task = new Task();
        task.setDescription(DESCRIPTION);
        task.setAssignee(assignee);
        task.setReviewer(reviewer);
        return task;
    }

    /**
     * Creates an Employee bean for the assignee with the specified name and position.
     *
     * @return An Employee bean for the assignee.
     */
    @Bean
    @Qualifier("assignee")
    public Employee assignee() {
        Employee employee = new Employee();
        employee.setName(ASSIGNEE_NAME);
        employee.setPosition(ASSIGNEE_POSITION);
        return employee;
    }

    /**
     * Creates an Employee bean for the reviewer with the specified name and position.
     *
     * @return An Employee bean for the reviewer.
     */
    @Bean
    @Qualifier("reviewer")
    public Employee reviewer() {
        Employee employee = new Employee();
        employee.setName(REVIEWER_NAME);
        employee.setPosition(REVIEWER_POSITION);
        return employee;
    }
}
