package com.epam.rd.autotasks;

import com.epam.rd.autotasks.bean.Task;
import com.epam.rd.autotasks.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class TaskMain {
    /**
     * The main method to run the application and demonstrate Spring bean usage.
     *
     * @param args Command-line arguments (not used in this example).
     */
    public static void main(String[] args) {
        // Create and initialize the Spring context based on the AppConfig class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        // Retrieve the Task bean from the context
        Task task = context.getBean(Task.class);

        // Display Task information
        System.out.println("Description: " + task.getDescription());
        System.out.println("Assignee : " + task.getAssignee().getName() + " - " + task.getAssignee().getPosition());
        System.out.println("Reviewer : " + task.getReviewer().getName() + " - " + task.getReviewer().getPosition());

        // Close the Spring context
        context.close();
    }
}
